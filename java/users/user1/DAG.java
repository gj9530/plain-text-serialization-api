package users.user1;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

public class DAG {
    public static char aChar = 'a';
    public String s = "test:result{}[]";
    private int i = 5;
    public boolean b = true;
    public final double aDouble = 1.0;
    static short aShort = 2;
    public byte aByte = 1;
    public long aLong = 30000000;
    BigDecimal value = new BigDecimal("1.2846202978398e+19");
    Number number = 987.3;
    BigInteger integer = new BigInteger("938858673886394");
    public float aFloat = 2;
    public String str = null;
    public Integer it = new Integer(1);
    public Node n1;
    public Node n2;
    public Node n3;
    public Supernode sn1;
    public Supernode sn2;
    public String[] strArray;
    public int[] num = new int[3];
    public Node[] n = new Node[2];
    public Object[] objects = new Object[5];
    public List<Integer>[] listarray = new List[2];
    public List<int[]> arrayList = new ArrayList<>();
    public List<Integer> lst = new ArrayList<>();
    public List<String> stringList = new ArrayList<>();
    public List<Object> objList = new ArrayList<>();
    public List<Boolean> boolList = new ArrayList<>();
    public int[][] twoDArray = new int[5][7];
    public List<Supernode> lst1 = new ArrayList<>();
    public Vector<Integer> vector;
    public Supernode[] sns = new Supernode[5];
     public List<List<Node>> nodes = new ArrayList<>();
    public List<Integer> aList = new ArrayList<>();
    public Collection<String> bPQ;
    public List<List<Integer>> bList;
    public List<Supernode> dList;
    public List<int[]> arrList;
    public Map<String, Integer> aMap;
    public Map<Integer, Node> id_node = new HashMap<>();
    public Map<Node, Integer> node_id;
    public Map<Object, Object> hashtable = new HashMap<>();
    public Set<Integer> aset = new HashSet<>();

    public DAG(){}
//
    //Make a graph with three nodes. Both n1 and n2 have the same neighbor n3.
//
    public void makeDAG(){
        n1 = new Node(1);
        n2 = new Node(2);
        Node n3 = new Node(3);
        n1.neighbor = n3;
        n2.neighbor = n3;


        strArray= new String[10];
        for(int i = 0; i < 3; i ++){
//            strArray[i] = "" + (i + 100);
            num[i] = i;
        }
        n[0] = new Node(999);
        n[1] = new Node(888);


        List<Integer> lst = new ArrayList<>();
        lst.add(10);
        lst.add(20);
        List<Integer> aLst = new LinkedList<>();
        aLst.add(80);
        aLst.add(99);
        listarray[0] = lst;
        listarray[1] = aLst;
        for(int i = 0; i < 5; i ++){
            for(int j = 0; j < 7; j ++){
                twoDArray[i][j] = i * 10 + j;
            }
        }

        stringList.add(null);
        stringList.add("a");
        stringList.add("b");
        objList.add(1);
        objList.add("a");
        objList.add(new Node(8776));
        objList.add(null);
        objList.add(true);
        boolList.add(true);
        boolList.add(false);
        boolList.add(null);
        Node n4 = new Node(4);
        Node n5 = new Node(5);
        Node n6 = new Node(6);
        Supernode sn1 = new Supernode(91);
        Supernode sn2 = new Supernode(92);

        lst1.add(n4);
        lst1.add(n5);
        lst1.add(n6);
        lst1.add(sn1);
        lst1.add(sn2);
        sns[0] = n4;
        sns[1] = n5;
        sns[2] = n6;
        sns[3] = sn1;
        sns[4] = sn2;
        objects[0] = 1;
        objects[1] = "a";
        objects[2] = new Node(121);
        objects[3] = false;


        Node n7 = new Node(7);
        Node n8 = new Node(8);
        Node n9 = new Node(9);
        List<Node> lst2 = new ArrayList<>();
        lst2.add(n7);
        lst2.add(n8);
        lst2.add(n9);
        nodes.add(lst2);
        aList = new ArrayList<>();
        aList.add(111);
        aList.add(222);
        List<Integer> cList = new ArrayList<>();
        cList.add(888);
        cList.add(999);
        bPQ = new PriorityQueue<>();
        bPQ.add("A");
        bPQ.add("B");
        bList = new ArrayList<>();
        bList.add(aList);
        bList.add(cList);
        dList = new ArrayList<>();
        dList.add(new Node(12345));
        arrList = new LinkedList<>();
        arrList.add(num);
        vector = new Vector<>();
        vector.add(33);
        vector.add(44);
        aMap = new HashMap<>();
        aMap.put("a" , 1);
        aMap.put("b" , 2);
        aMap.put(null, 3);
        id_node = new HashMap<>();
        node_id = new LinkedHashMap<>();
        Node n11 = new Node(11);
        Node n12 = new Node(12);
        Node n13 = null;
        id_node.put(1, n1);
        id_node.put(2, n2);
        id_node.put(11, n11);
        id_node.put(12, n12);
        id_node.put(13, n13);

        node_id.put(n1, 1);
        node_id.put(n2, 2);
        node_id.put(n12, 12);
        node_id.put(n13, 13);

        hashtable.put("a", 1);
        hashtable.put(1, "b");
        hashtable.put(true, null);
        hashtable.put(null, false);

        aset.add(101);
        aset.add(202);
        aset.add(303);


    }
}
