package users.user1;

import edu.rit.cs.serializer.JSONAPI;


public class Main2 {
    void finddifference(String s1, String s2){
        int index = 0;
        for (int i = 0; i < s1.length(); i ++){
            if (s1.charAt(i) == s2.charAt(i)){
                continue;
            }
            index = i;
            System.out.println(s1.substring(i));
            break;
        }
    }
    public static void main(String[] args){
        DAG d = new DAG();
        d.makeDAG();

        JSONAPI api = new JSONAPI();
        String jsonString = api.toJson(d);
        System.out.println(jsonString);
        System.out.println("===============================");
//        api.writeToFile(jsonString);
        JSONAPI api2 = new JSONAPI();
//        String str = api2.readFromFile();
        Object dag = api2.fromJson(jsonString);
        JSONAPI api3 = new JSONAPI();
        String jsonString2 = api3.toJson(dag);
        System.out.println(jsonString2);
        if(jsonString.equals(jsonString2)){
            System.out.println("The object created by the derialization is the same as the original object");
        }
        else{

            System.out.println("The object created by the derialization is NOT the same as the original object");
//            new Main2().finddifference(jsonString, jsonString2);
        }
    }
}

