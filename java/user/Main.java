package user;

import edu.rit.cs.serializer.JSONAPI;
import users.user1.Main2;

public class Main {
    public static void main(String[] args){

        DAG d = new DAG();
        d.makeDAG();
        JSONAPI api = new JSONAPI();
        String jsonString = api.toJson(d);
        System.out.println(jsonString);
        System.out.println("===============================");
        api.writeToFile("C:/Users/jingu/IdeaProjects/JSON/json.txt", jsonString);

    }
}
