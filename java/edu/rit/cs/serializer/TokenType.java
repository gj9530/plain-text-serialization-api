package edu.rit.cs.serializer;


/**
 * This class enumerates all the token types.
 * @author Guangze Jin
 */
enum TokenType {
    SERIALIZER, ID, LBRACE, RBRACE, LBRACKET, RBRACKET, COLON, COMMA, CLASS, NEWREF, REFID, LENGTH, ELEMENTTYPE, KEYTYPE, VALUETYPE;
}