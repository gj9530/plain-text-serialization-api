package edu.rit.cs.serializer;

/**
 * This class is used to create tokens for the Lexer class and the Deserializer class. Two class variables are assigned
 * by the constructor.
 * @author Guangze Jin
 */

class Token {
    private TokenType type;
    private String id;
    Token(TokenType type, String id){
        this.type = type;
        this.id = id;
    }

    /**
     * This method returns the type of the token.
     * @return the type of the token.
     */
    TokenType getType(){
        return type;
    }

    /**
     * This method returns identification in a string format.
     * @return a string.
     */
    String getId(){
        return id;
    }



}
