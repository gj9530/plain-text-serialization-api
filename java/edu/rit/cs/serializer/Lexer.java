package edu.rit.cs.serializer;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to tokenize Smoosh strings.
 * @author Guangze Jin
 */
class Lexer {
    private List<String> stringList = new ArrayList<>();
    private String jsonString;
    Lexer(String jsonString){
        this.jsonString = jsonString;
    }

    private String addQuote(String str){
        return "\"" + str + "\"";
    }

    /**
     * This method removes meanless characters such as white spaces or new line signs. The characters in the JSON
     * string are organized to the meaningful strings that are inserted into a list.
     * @return a list of strings
     */
    List<String> makeStringArray(){
        boolean inQuote = false;
        StringBuilder sb = new StringBuilder();
        int index = 0;
        int length = jsonString.length();
        while(index < length){
            char c = jsonString.charAt(index);
            if(c == '"' && inQuote){         //This if/else statement is checking the content of a string
                sb.append(c);                // inside of a pair of double quotes, but it cannot handle the strings with
                stringList.add(sb.toString()); // double quotes in them.
                inQuote = false;
                sb = new StringBuilder();
            }
            else if(c == '"' && !inQuote){
                if (sb.length() > 0){
                    stringList.add(sb.toString());
                    sb = new StringBuilder();
                    sb.append(c);
                }
                else{
                    sb.append(c);
                }
                inQuote = true;
            }
            else if(inQuote){
                sb.append(c);
            }
            else if(c == ' ' || c == '\n' || c == '\r'){
                index ++;
                continue;
            }
            else if (isSymbol(c)){
                if (sb.length() > 0){
                    stringList.add(sb.toString());
                    sb = new StringBuilder();
                }
                sb.append(c);
                stringList.add(sb.toString());
                sb = new StringBuilder();
            }
            else{
                sb.append(c);
            }
            index ++;
        }
        return stringList;
    }

    /**
     * This method check if the character is a symbol such as ',', '[', '{', etc..
     * @param c the character to be checked.
     * @return true if the character is a symbol; otherwise, return false.
     */
    private boolean isSymbol(char c){
        int size = stringList.size();
        if(c == '[' && size > 2 && (stringList.get(size - 2).equals(addQuote("@class")) || stringList.get(size - 2).equals(addQuote("@elementType")) || stringList.get(size - 2).equals(addQuote("@keyType")) || stringList.get(size - 2).equals(addQuote("@valueType")))){
            return false;
        }
        return c == ',' || c == ':' || c == '{' || c == '}' || c == '[' || c == ']';
    }

    /**
     * This method make a list of tokens based on the list of the strings.
     * @param stringArray is the list of the strings.
     * @return a list of tokens.
     */
    List<Token> makeTokens(List<String> stringArray){
        List<Token> tokens = new ArrayList<>();
        for (int i = 0; i < stringArray.size(); i ++){
            String s = stringArray.get(i);
            if(s.equals(" ") || s.equals("\n")){
                continue;
            }
            if(s.equals(",")){
                tokens.add(getToken(TokenType.COMMA, null));
            }
            else if(s.equals(":")){
                tokens.add(getToken(TokenType.COLON, null));
            }
            else if(s.equals("{")){
                tokens.add(getToken(TokenType.LBRACE, null));
            }
            else if(s.equals("}")){
                tokens.add(getToken(TokenType.RBRACE, null));
            }
            else if(s.equals("[")){
                tokens.add(getToken(TokenType.LBRACKET, null));
            }
            else if(s.equals("]")){
                tokens.add(getToken(TokenType.RBRACKET, null));
            }
            else if(s.equals(addQuote("@serializer"))){
                tokens.add(getToken(TokenType.SERIALIZER, null));
            }
            else if(s.equals(addQuote("@class"))){
                tokens.add(getToken(TokenType.CLASS, null));
            }
            else if(s.equals(addQuote("@referenceID"))){
                tokens.add(getToken(TokenType.NEWREF, null));
            }
            else if(s.equals(addQuote("@length"))){
                tokens.add(getToken(TokenType.LENGTH, null));
            }
            else if(s.equals(addQuote("@elementType"))){
                tokens.add(getToken(TokenType.ELEMENTTYPE, null));
            }
            else if(s.equals(addQuote("@keyType"))){
                tokens.add(getToken(TokenType.KEYTYPE, null));
            }
            else if(s.equals(addQuote("@valueType"))){
                tokens.add(getToken(TokenType.VALUETYPE, null));
            }
            else if(s.charAt(0) == '('){
                String refId = s.substring(15);
                tokens.add(getToken(TokenType.REFID, refId));
            }
            else{
                tokens.add(getToken(TokenType.ID, s));
            }
        }
        return tokens;
    }


    /**
     * This method generates the instances of token class.
     * @param type is the type of the token such TokenType.ID, TokenType.COMMA, etc..
     * @param id is the name or the value of ID.
     * @return an instance of Token class.
     */
    private Token getToken(TokenType type, String id){
        Token token = new Token(type, id);
        return token;
    }

}
