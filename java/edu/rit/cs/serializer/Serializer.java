package edu.rit.cs.serializer;

import java.lang.reflect.*;
import java.util.*;


/**
 * This class is used to convert objects to Smoosh strings.
 *<p>
 * Each class object has a name-value pair in the string, which is represented as "@referenceID: idcouter". The
 * idcouter is an integer that is used as the unique id number for each object. If the object is accessed again, its
 * value will be displayed as "name: (@referenceID) = idcounter ". This method avoids generating multiple references
 * for the same object when users are deserializing Smoosh strings.
 *</p>
 * @author Guangze Jin
 */

class Serializer {

    private int idCounter = 0;
    private int level = 0;
    private HashMap<Object, Integer> obj_idCounter = new HashMap<>();


    /**
     * This method increase the indentation level in JSON strings.
     */
    private void incLevel()
    {
        level = level + 2;
    }

    /**
     * This method decrease the indentation level in JSON strings.
     */
    private void decLevel() {
        level = level - 2;
    }

    /**
     * This method generates white spaces based on the indentation level.
     * @return a string of several white spaces based on the indentation level.
     */
    private String printInc() {
        char[] chars = new char[level];
        java.util.Arrays.fill(chars, ' ');
        return new String(chars);
    }

    /**
     * This method removes the extra comma and new line sign from a stringBuilder.
     * @param stringBuilder is the stringBuilder with extra comma and new line sign.
     * @return a stringbuilder after removing the extra comma and new line sign.
     */

    private StringBuilder rmTail(StringBuilder stringBuilder){
        StringBuilder sb = stringBuilder;
        while (sb.length() != 0){
            if (sb.charAt(sb.length() - 1) != '\n' && sb.charAt(sb.length() - 1) != ',' && sb.charAt(sb.length() - 1) != ' '){
                break;
            }
            sb.setLength(sb.length() - 1);
        }
        return sb;
    }


    /**
     * This method removes the extra comma and new line sign.
     * @param str is the string with extra comma and new line sign.
     * @return a string after removeing the extra comma and new line sign.
     */
    private String rmTail(String str){
        String s = str;
        while (s.length() != 0){
            if (s.charAt(s.length() - 1) != '\n' && s.charAt(s.length() - 1) != ',' && s.charAt(s.length() - 1) != ' '){
                break;
            }
            s = s.substring(0, s.length() - 1);
        }
        return s;
    }

    /**
     * Add quote to a string.
     * @param str is a string that will be quoted.
     * @return the string with quote.
     */
    private String addQuote(String str){
        return "\"" + str + "\"";
    }


    /**
     * This method check if the object is the subclass of java.lang.Number class.
     * @param object is the object to be checked.
     * @return true if the object is the subclass of java.lang.Number class, return true; otherwise, return false.
     */
    private boolean isNumber(Object object) {
        if (object == null) {
            return false;
        }
        Class clazz = object.getClass();
        return Number.class.isAssignableFrom(clazz);
    }

    /**
     * This method check the data type of the object and pass it to the method that specifically process the object data type.
     * @param object to be processed.
     * @return JSON string generated from the object.
     */

    String serialize(Object object){
        String str = "";
        if(obj_idCounter.containsKey(object)){
            return "(@referenceID)" + " = " + obj_idCounter.get(object) + ",\n";
        }
        if(object == null){
            return "null,\n";
        }
        if(isNumber(object) || object.getClass() == Boolean.class){
            return String.valueOf(object) + ",\n";
        }
        if(object.getClass() == String.class || object.getClass() == Character.class){
            return addQuote(String.valueOf(object)) + ",\n";
        }
        if(object instanceof Collection){
            str = "[\n";
            incLevel();
            String fromCollection = serializeCollection(object);
            Object className = object.getClass().getName();
            str = str + printInc() + addQuote("@class") + ": " + className + ",\n";
            str = str + fromCollection;
            decLevel();
            str = rmTail(str);
            str = str + "\n" + printInc() + "],\n";
            return str;
        }
        if(object.getClass().isArray()){
            str = "[\n";
            incLevel();
            String fromArray = serializeArray(object);
            Object arrayType = object.getClass().getName();
            str = str + printInc() + addQuote("@class") + ": " + arrayType + ",\n";
            str = str + fromArray;
            decLevel();
            str = rmTail(str);
            str = str + "\n" + printInc() + "], \n";
            return str;
        }
        if(object instanceof Map){
            str = "{\n";
            incLevel();
            String fromMap = serializeMap(object);
            Object className = object.getClass().getName();
            str = str + printInc() + addQuote("@class") + ": " + className + ",\n";
            str = str + fromMap;
            decLevel();
            str = rmTail(str);
            str = str + "\n" + printInc() + "},\n";
            return str;
        }
        str = serializeObject(object);
        return str;
    }

    /**
     * This method converts collection objects to JSON format. The fieldValue object is casted to a wildcard
     * collection data type. An iterator is used to iterate over the collection. Each element in the collection is
     * converted to a string. A stringbuilder is used to combine all the generated strings.
     *
     * @param fieldValue is an collection object that is to be converted to JSON format.
     * @return a JSON formatted string that the fieldValue object is converted to.
     */
    private String serializeCollection(Object fieldValue) {
        StringBuilder sb = new StringBuilder();
        Collection<?> collection = (Collection<?>) fieldValue;
        Iterator<?> it = collection.iterator();
        String className = "null";
        while (it.hasNext()){
            Object next = it.next();
            if(next == null) continue;
            try {
                if(className.equals("null") || next.getClass().isAssignableFrom(Class.forName(className))){
                    className = next.getClass().getName();
                }
                else{
                    className = Object.class.getName();
                    break;
                }

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        sb.append(printInc() + addQuote("@elementType") + ": "  + className + ",\n");
        it = collection.iterator();

        while(it.hasNext()){
            Object next = it.next();
                String s = serialize(next);
                sb.append(printInc() + s);
        }
        sb = rmTail(sb);
        sb.append(printInc() + "\n");
        return sb.toString();
    }

    /**
     * This method converts collection objects to JSON format. The fieldValue object is casted to an object Array.
     * Each element in the array is accessed using a for loop and is converted to a string that is . A stringbuilder
     * is used to combine all the generated json strings.
     *
     * @param fieldValue is an array object that is to be converted to JSON format.
     * @return a JSON formatted string that the fieldValue object is converted to.
     */
    private String serializeArray(Object fieldValue) {
        StringBuilder sb = new StringBuilder();
        int length = Array.getLength(fieldValue);

        sb.append(printInc() + addQuote("@length") + ": " + length + ",\n");
        for(int i = 0; i < length; i ++){
            Object object = Array.get(fieldValue, i);
            if (object != null){
                String s = serialize(object);
                sb.append(printInc() + s);
            }
        }

        sb = rmTail(sb);
        sb.append(printInc() + "\n");
        return sb.toString();
    }


    /**
     * This method converts a map to JSON.
     * @param fieldValue is the object to be serialized.
     * @param <K> the data type of the keys.
     * @param <V> the data type of the values.
     * @return a JSON string that is generated from a map object.
     */
    private<K, V> String serializeMap(Object fieldValue) {
        StringBuilder sb = new StringBuilder();
        Map<K, V> aMap = (Map<K, V>) fieldValue;
        String keyType = "null";
        String valueType = "null";
        for(Map.Entry<K, V> pair : aMap.entrySet()){
            K key = pair.getKey();
            if(key == null) continue;
            try {
                if(keyType.equals("null") || key.getClass().isAssignableFrom(Class.forName(keyType))) {
                    keyType = key.getClass().getName();
                }
                else{
                    keyType = Object.class.getName();
                    break;
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        for(Map.Entry<K, V> pair : aMap.entrySet()){
            V value = pair.getValue();
            if(value == null) continue;
            try {
                if(valueType.equals("null") || value.getClass().isAssignableFrom(Class.forName(valueType))) {
                    valueType = value.getClass().getName();
                }
                else{
                    valueType = Object.class.getName();
                    break;
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        sb.append(printInc() + addQuote("@keyType") + ": " + keyType + ",\n");
        sb.append(printInc() + addQuote("@valueType") + ": " + valueType + ",\n");
        for(Map.Entry<K, V> pair : aMap.entrySet()){
            K key = pair.getKey();
            V value = pair.getValue();
            String str_key = rmTail(serialize(key));
            String str_value = serialize(value);
            sb.append(printInc() + str_key + ": " + str_value);
        }
        sb = rmTail(sb);
        sb.append(printInc() + "\n");
        return sb.toString();
    }

    /**
     * This method converts all types of objects to JSON format. First, check the type of this object. If the object
     * is a primitive type, a string, an array or a collection, it indicates the object is from a field value. In
     * this case, no field name is needed. If the object is an instance of a class, the fields of the object can be
     * obtained using Java Reflection API. The resulting string of the class instance is a JSON formatted name-value
     * pair. The field value is processed based on its data type. If the field value is an array or a collection, it
     * can be processed by serializeArray and serializeCollection respectively. If the field value is a class
     * instance, it can be processed by recursive call of this method.
     *
     * @param obj is the object that is to be converted to a JSON formatted string.
     * @return the json formatted string that the object is converted to.
     */
    private String serializeObject(Object obj){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{\n");
        incLevel();
        String objectName = obj.getClass().getName();
        stringBuilder.append(printInc() + addQuote("@class") + ": " + objectName + ",\n");
        stringBuilder.append(printInc() + addQuote("@referenceID") + ": " + idCounter + ",\n");
        obj_idCounter.put(obj, idCounter++);
        Class objClass = obj.getClass();
        Field[] fields = objClass.getDeclaredFields();
        for (Field field : fields){
            if(Modifier.isStatic(field.getModifiers())){
                continue;
            }
            field.setAccessible(true);  //get access to the private and protected fields
            String fieldName = field.getName();
            Object fieldValue = null;
            try {
                fieldValue = field.get(obj);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if (fieldValue == null){
                stringBuilder.append(printInc() + addQuote(fieldName) + ": " + "null,\n");
            }
            else if(isNumber(fieldValue) || fieldValue.getClass() == Boolean.class ){
                stringBuilder.append(printInc() + addQuote(fieldName) + ": " + fieldValue + ",\n");
            }
            else if(fieldValue.getClass() == String.class || fieldValue.getClass() == Character.class){
                stringBuilder.append(printInc() + addQuote(fieldName) + ": " + addQuote(fieldValue.toString()) + ",\n");
            }
            else if(obj_idCounter.containsKey(fieldValue)){
                stringBuilder.append(printInc() + addQuote(fieldName) + ": (@referenceID)" + " = " + obj_idCounter.get(fieldValue) + ",\n");
            }
            else if(fieldValue instanceof Collection){
                stringBuilder.append(printInc() + addQuote(fieldName) + ": [\n");
                incLevel();
                String fromCollection = serializeCollection(fieldValue);
                Object className = fieldValue.getClass().getName();
                stringBuilder.append(printInc() + addQuote("@class") + ": " + className + ",\n");
                stringBuilder.append(fromCollection);
                stringBuilder = rmTail(stringBuilder);
                stringBuilder.append("\n");
                decLevel();
                stringBuilder.append(printInc() + "],\n");

            }
            else if((fieldValue.getClass().isArray())){
                stringBuilder.append(printInc() + addQuote(fieldName) + ": [\n");
                incLevel();
                String fromArray = serializeArray(fieldValue);
                String arrayType = fieldValue.getClass().getName();
                stringBuilder.append(printInc() + addQuote("@class") + ": " + arrayType + "," +
                                     "\n");
                stringBuilder.append(fromArray);
                stringBuilder = rmTail(stringBuilder);
                stringBuilder.append("\n");
                decLevel();
                stringBuilder.append(printInc() + "],\n");
            }
            else if(fieldValue instanceof Map){
                stringBuilder.append(printInc() + addQuote(fieldName) + ": {\n");
                incLevel();
                String fromMap = serializeMap(fieldValue);
                Object className = fieldValue.getClass().getName();
                stringBuilder.append(printInc() + addQuote("@class") + ": " + className + ",\n");
                stringBuilder.append(fromMap);
                stringBuilder = rmTail(stringBuilder);
                stringBuilder.append("\n");
                decLevel();
                stringBuilder.append(printInc() + "},\n");


            }
            else{
                String nestedObj = serializeObject(fieldValue);
                stringBuilder.append(printInc() + addQuote(fieldName) + ": " + nestedObj);
            }
        }
        String jsonString = rmTail(stringBuilder).toString();
        decLevel();
        jsonString = jsonString + "\n" + printInc() + "},\n";

        return jsonString;
    }



}
