package edu.rit.cs.serializer;

import java.lang.reflect.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;


/**
 * This class deserializes the list of the tokens and create a class object.
 *<p>Deserializing the Smoosh string:</p>
 *
 * <p>grab a LBRACE token //the first left brace</p>
 * <p>grab a NEWREF token</p>
 * <p>grab a COLON token</p>
 * <p>grab an ID token</p>
 * <p>grab a COMMA token// these four tokens are the reference id for the object, which is useless</p>
 * <p>grab an ID token // can get variable name from this token.</p>
 * <p>grab a COLON token</p>
 * <p>If current token is "[", invoke the deserializeCollection or the deserializeArray methods based on the id of the CLASS token.</p>
 * <p>If current token is "{", invoke the deserializeObject or deserializeMap methods based on the id of the CLASS token .</p>
 * <p>If current token is a scalar value or a string, cast the value of the token to the correct type and set the field value</p>
 * <p>grab COMMA token</p>
 * <p> repeat grabbing the ID tokens and the COMMA tokens until a RBRACE token is met instead of a COMMA token.</p>
 * <p>   If the object has another object inside, recursively call the deserialize() method.</p>
 * @author Guangze Jin
 */

class Deserializer {
    private List<Token> tokens;
    private int index;
    private Token token;
    private Map<Integer, Object> idCounter_object = new HashMap<>();

    /**
     * The constructor is to pass and update the token list and assign the first element in the token.
     * list to the current token token.
     * @param tokens is the original token list for deserialization.
     */
    Deserializer(List<Token> tokens){
        this.tokens = update_tokens(tokens);
        index = 0;
        token = tokens.get(index);
    }

    /**
     * This method converts the original token list to its java version.
     * @param tokens is the original token list for deserialization.
     * @return the java version of the token list.
     */
    private List<Token> update_tokens(List<Token> tokens) {
        List<Token> new_tokens = new ArrayList<>();
        for (Token token : tokens){
            if (token.getType() == TokenType.ID){
                String id = token.getId();
                Token new_token = python2java(id);
                new_tokens.add(new_token);
            }
            else{
                new_tokens.add(token);
            }
        }
        return new_tokens;
    }

    /**
     * This method converts python data types to java data types and creates
     * a new token with a java data type
     * @param id is the string representing the data type from python.
     * @return a new token.
     */
    private Token python2java(String id) {
        String type = id;
        if (type.equals("int")){
            type = "java.lang.Integer";
        }
        if (type.equals("float")){
            type = "java.lang.Double";
        }
        if (type.equals("object")){
            type = "java.lang.Object";
        }
        if (type.equals("list") || type.equals("tuple") || type.equals("range")){
            type = "java.util.ArrayList";
        }
        if (type.equals("dict")){
            type = "java.util.HashMap";
        }
        if (type.equals("set") || type.equals("frozenset")){
            type = "java.util.HashSet";
        }
        if (type.equals("str")){
            type = "java.lang.String";
        }
        if (type.equals("bool")){
            type = "java.lang.Boolean";
        }
        if (type.equals("True") || type.equals("False")){
            type = type.toLowerCase();
        }
        if (type.equals("complex") || type.equals("bytes") || type.equals("bytearray") || type.equals("memoryview")){
            String errorMessage = "This API can NOT process the data type of \"" + type + "\"";
            try {
                throw new TypeException(errorMessage);
            } catch (TypeException e) {
                e.printStackTrace();
                System.exit(1);
            }
        }
        return new Token(TokenType.ID, type);
    }

    /**
     * This method moves token to the next token in the token list.
     */
    private void nextToken(){
        index ++;
        if(index < tokens.size()){
            token = tokens.get(index);
        }
    }

    /**
     * This method move to the previous token in the token list.
     */
    private void previousToken(){
        index --;
        if (index < 0) index = 0;
        token = tokens.get(index);
    }

    /**
     * This method remove the quote from a string.
     * @param str is the string without the quote.
     * @return the string after removing the quote.
     */
    private String rmQuote(String str){
        if(str.charAt(0) == '\"' && str.charAt(str.length() - 1) == '\"'){
            return str.substring(1, str.length() - 1);
        }
        return str;
    }


    /**
     * This method checks if the token matches the given type and move token to the next token.
     * @param type is a token type the the current token token wants to match.
     * @return the token tk before it moves to the next token.
     */
    private Token matchToken(TokenType type){
//        if (token.getType() == TokenType.ID){
//            System.out.println(token.getType() + "(" + token.getId() +")" + "   " + type);
//        }
//        else{
//            System.out.println(token.getType()  + "   " + type);
//        }
        Token eatenToken = token;
        if(token.getType() == type){
            nextToken();
        } else{
            String errorMessage = "Current Token type is " + token.getType() + ", which does not match type " + type;
            try {
                throw new TypeException(errorMessage);
            } catch (TypeException e) {
                e.printStackTrace();
                System.exit(1);
            }

        }
        return eatenToken;
    }

    /**
     * Check if a class is the subclass of java.lang.Number class.
     * @param clazz the class to be checked.
     * @return true if the class is the subclass of java.lang.Number.
     */
    private  boolean isNumber(Class<?> clazz) {
        return Number.class.isAssignableFrom(clazz);
    }


    /**
     * This method check if the class is primitive and return the corresponding wrapper class.
     * @param clazz the class to be checked.
     * @return the wrapper class of a primitive data type.
     */
    private Class<?> getWapperClass(Class<?> clazz) {
        String type = clazz.toString();
        if(type.equals("int")) return Integer.class;
        if(type.equals("byte")) return Byte.class;
        if(type.equals("short")) return Short.class;
        if(type.equals("long")) return Long.class;
        if(type.equals("float")) return Float.class;
        if(type.equals("double")) return Double.class;
        if(type.equals("char")) return Character.class;
        if(type.equals("boolean")) return Boolean.class;

        return clazz;
    }

    /**
     * This method identifies the data type of the current json representation.
     * @return the class name of the data type.
     */
    private Class<?> identifyType() {
        if(token.getType() == TokenType.LBRACKET || token.getType() == TokenType.LBRACE){
            nextToken();
            nextToken();
            nextToken();
            Class<?> clazz = null;
            try {
                clazz = Class.forName(token.getId());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            previousToken();
            previousToken();
            previousToken();
            return clazz;
        }

        else if(token.getId().equals("null")){
            return null;
        }
        else if(token.getId().charAt(0) == '"' && token.getId().charAt(token.getId().length() - 1) == '"'){
            return String.class;
        }
        else if(token.getId().equals("true") || token.getId().equals("false")){
            return Boolean.class;
        }
        else if(isBigInteger(token.getId())){
            return BigInteger.class;
        }

        else if(isBigDecimal(token.getId())){
            return BigDecimal.class;
        }
        else{
            System.out.println(token.getId() + " does NOT match any data type.");
            System.exit(1);
        }
        return null;
    }

    /**
     * This method checks if a given string is a valid integer.
     * @param id the given string
     * @return true if the given string is a valid integer and false otherwise.
     */
    private boolean isBigInteger(String id) {
        try
        {
            new BigInteger(id);
        }
        catch (NumberFormatException e)
        {
            return false;
        }
        return true;

    }

    /**
     * This method checks if a given string is a valid number.
     * @param id the given string
     * @return true if the given string is a valid number and false otherwise.
     */
    private boolean isBigDecimal(String id) {
        try
        {
            new BigDecimal(id);
        }
        catch (NumberFormatException e)
        {
            return false;
        }
        return true;
    }



    /*
     * This method checks the type of the class and pass it to the method that processes this specific type.
     * @param clazz is the class to be checked.
     * @return deserialized object.
     */
    Object deserialize(Class<?> clazz) {
        if (token.getType()==TokenType.ID && token.getId().equals("null")){
            matchToken(TokenType.ID);
            return null;
        }

        if (token.getType() == TokenType.REFID){
            int counter = Integer.valueOf(token.getId());
            matchToken(TokenType.REFID);
            return idCounter_object.get(counter);
        }

        Object value = null;
        if (clazz.isPrimitive() || clazz == Boolean.class || clazz == String.class || isNumber(clazz)){
            value = deserializeSingleValue(clazz, matchToken(TokenType.ID).getId());
        }

        else if(Collection.class.isAssignableFrom(clazz)){
            value = deserializeCollection();
        }

        else if(clazz.isArray()){
            value = deserializeArray();
        }

        else if(Map.class.isAssignableFrom(clazz)){
            value = deserializeMap();
        }
        else{
            value = deserializeObject(clazz);
        }
        return value;
    }

    /**
     * This method is used to cast an object to a primitive type.
     * @param fieldType the object to be cast.
     * @param jsonValue the actual value in a string format.
     * @return a primitive or string format of the value.
     */
    private Object deserializeSingleValue(Object fieldType, String jsonValue) {
        if(jsonValue.equals("null")) return null;
        if(fieldType == boolean.class || fieldType == Boolean.class ) return Boolean.parseBoolean(jsonValue);
        if(fieldType == byte.class || fieldType == Byte.class) return Byte.parseByte(jsonValue);
        if(fieldType == short.class || fieldType == Short.class) return Short.parseShort(jsonValue);
        if(fieldType == int.class || fieldType == Integer.class ) return Integer.parseInt(jsonValue);
        if(fieldType == long.class || fieldType == Long.class) return Long.parseLong(jsonValue);
        if(fieldType == float.class || fieldType == Float.class) return Float.parseFloat(jsonValue);
        if(fieldType == double.class || fieldType == Double.class) return Double.parseDouble(jsonValue);
        if(fieldType == char.class || fieldType == Character.class) return jsonValue.charAt(1);
        if(fieldType == Number.class) {
            try {
                return NumberFormat.getInstance().parse(jsonValue);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        if(fieldType == BigDecimal.class) return new BigDecimal(jsonValue);
        if(fieldType == BigInteger.class) return new BigInteger(jsonValue);
        return rmQuote(jsonValue);
    }

    /**
     * This method deserializes a sequence of the tokens that reprensents an array.
     * @return the array that generated from the sequence of the tokens.
     */
    private Object deserializeArray() {
        matchToken(TokenType.LBRACKET);
        matchToken(TokenType.CLASS);
        matchToken(TokenType.COLON);
        String array_className = matchToken(TokenType.ID).getId();
        Class<?> clazz = null;

        try {
            clazz = Class.forName(array_className).getComponentType();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        matchToken(TokenType.COMMA);
        matchToken(TokenType.LENGTH);
        matchToken(TokenType.COLON);
        int length = Integer.valueOf(matchToken(TokenType.ID).getId());
        Object obj = Array.newInstance(clazz, length);
        if (token.getType()== TokenType.RBRACKET) {
            matchToken(TokenType.RBRACKET);
            return obj;
        }

        int position = 0;
        while(token.getType() != TokenType.RBRACKET){
            matchToken(TokenType.COMMA);
            if(clazz != Object.class){
                Array.set(obj, position++, deserialize(getWapperClass(clazz)));
            }
            else{
                Array.set(obj, position++, deserialize(identifyType()));
            }


        }
        matchToken(TokenType.RBRACKET);
        return obj;
    }


    /**
     * This method deserializes a sequence of the tokens that reprensents a collection.
     * @return the collection that generated from the sequence of the tokens.
     */
    private Object deserializeCollection() {
        matchToken(TokenType.LBRACKET);
        matchToken(TokenType.CLASS);
        matchToken(TokenType.COLON);
        String collection_className = matchToken(TokenType.ID).getId();
        matchToken(TokenType.COMMA);
        matchToken(TokenType.ELEMENTTYPE);
        matchToken(TokenType.COLON);
        String element_className = matchToken(TokenType.ID).getId();
        Class<?> clazz = null;
        Class<?> element_class = null;
        Collection<Object> collection = null;
        try {
            clazz =  Class.forName(collection_className);
            collection = (Collection<Object>) clazz.newInstance();
            if(!element_className.equals("null")){
                element_class = Class.forName(element_className);
            }

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }


        if (token.getType() == TokenType.RBRACKET) {
            matchToken(TokenType.RBRACKET);
            return collection;
        }

        while(token.getType() != TokenType.RBRACKET){
            matchToken(TokenType.COMMA);
            if(element_class == Object.class){
                collection.add(deserialize(identifyType()));
            }
            else{
                collection.add(deserialize(element_class));
            }

        }

        matchToken(TokenType.RBRACKET);
        return collection;
    }

    /**
     * This method deserializes a sequence of the tokens that reprensents a map.
     * @return the map that generated from the sequence of the tokens.
     */
    private Object deserializeMap(){
        matchToken(TokenType.LBRACE);
        matchToken(TokenType.CLASS);
        matchToken(TokenType.COLON);
        String map_className = matchToken(TokenType.ID).getId();
        matchToken(TokenType.COMMA);
        matchToken(TokenType.KEYTYPE);
        matchToken(TokenType.COLON);
        String keyTypeName = matchToken(TokenType.ID).getId();
        matchToken(TokenType.COMMA);
        matchToken(TokenType.VALUETYPE);
        matchToken(TokenType.COLON);
        String valueTypeName = matchToken(TokenType.ID).getId();
        Class<?> clazz = null;
        Class<?> keyType = null;
        Class<?> valueType = null;
        Map<Object, Object> map = null;
        try {
            clazz = Class.forName(map_className);
            map = (Map<Object, Object>) clazz.newInstance();
            if(!keyTypeName.equals("null")){
                keyType = Class.forName(keyTypeName);
            }

            if(!valueTypeName.equals("null")){
                valueType = Class.forName(valueTypeName);
            }

        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        if (token.getType() == TokenType.RBRACE) {
            matchToken(TokenType.RBRACE);
            return map;
        }

        while(token.getType() != TokenType.RBRACE){
            matchToken(TokenType.COMMA);
            Object key = null;
            Object value = null;
            if(keyType == Object.class){
                key = deserialize(identifyType());
            }
            else{
                key = deserialize(keyType);
            }
            matchToken(TokenType.COLON);
            if(valueType == Object.class){
                value = deserialize(identifyType());
            }
            else{
                value = deserialize(valueType);
            }
            map.put(key, value);
        }

        matchToken(TokenType.RBRACE);
        return map;
    }

    /**
     * This method is to process the token list and created an instance of a class.
     * @param clazz is the class of the object that users want to convert the JSON string back to.
     * @return an instance of the givien class.
     */
    private Object deserializeObject(Class<?> clazz){
        if (token.getType() == TokenType.REFID){
            int counter = Integer.valueOf(token.getId());
            matchToken(TokenType.REFID);
            return idCounter_object.get(counter);
        }

        matchToken(TokenType.LBRACE);
        matchToken(TokenType.CLASS);
        matchToken(TokenType.COLON);
        Class<?> new_clazz = null;
        try {
            new_clazz = Class.forName(matchToken(TokenType.ID).getId());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        matchToken(TokenType.COMMA);
        matchToken(TokenType.NEWREF);
        matchToken(TokenType.COLON);
        int idCounter = Integer.valueOf(matchToken(TokenType.ID).getId());

        Object newObj = null;
        try {
            newObj = new_clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        idCounter_object.put(idCounter, newObj);

        if(token.getType() != TokenType.RBRACE){
            matchToken(TokenType.COMMA);
        }

        Field[] fields = new_clazz.getDeclaredFields();
        for(Field field : fields){
            if(Modifier.isStatic(field.getModifiers())){
                continue;
            }
            field.setAccessible(true);
            String variableName = rmQuote(matchToken(TokenType.ID).getId());
            String fieldName = field.getName();
            if (!variableName.equals(fieldName)){
                String errorMessage = "Can not set the value of \"" + variableName + "\" to the field of \"" + fieldName + "\"";
                try {
                    throw new TypeException(errorMessage);
                } catch (TypeException e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }

            Object fieldValue = null;
            matchToken(TokenType.COLON);
            fieldValue = deserialize((Class)field.getType());

            try {
                field.set(newObj, fieldValue);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            if(token.getType() != TokenType.RBRACE){
                matchToken(TokenType.COMMA);
            }
        }
        matchToken(TokenType.RBRACE);

        return newObj;
    }

    class TypeException extends Exception{
        TokenType type1;
        TokenType type2;
        TypeException(String errorMessage){
            super(errorMessage);
        }
    }

}
