package edu.rit.cs.serializer;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * This class is provided to users to perform serializaton and deserialization.
 * @author Guangze Jin
 */
public class JSONAPI {
    /** This is the method for users to serialize objects.
     *
     * @param object is the object to be serialized.
     * @return the json formatted string that the object is converted to.
     */
    public String toJson(Object object) {
        return serialize(object);

    }


    /**
     * this is the method for uses to convert the JSON string back to objects
     * @param jsonString is the JSON formatted string that is used to generate objects.
     * @return an instance of clazz
     */
    public Object fromJson(String jsonString){
        return deserialize(jsonString);
    }


    /**
     * This method invoke the serialize() function in the Serializer class.
     * @param object to be serialized
     * @return the JSON string by serializing the object.
     */
    private String serialize(Object object){
        Serializer serializer = new Serializer();
        String jsonString = serializer.serialize(object);
        return jsonString.substring(0, jsonString.length() - 2);
    }

    /**
     * This method converts the JSON string to objects. This method use Lexer class to generate a list of tokens
     * based on the JSON string.  class parses the token list to generate the instance of the clazz class.
     * @param jsonString is the JSON formatted string that is used to generate objects.
     * @return an instance of clazz
     */
    private Object deserialize(String jsonString){
        Lexer lexer = new Lexer(jsonString);
        List<String> strings = lexer.makeStringArray();
        List<Token> tokens = lexer.makeTokens(strings);
        Deserializer deserializer = new Deserializer(tokens);
        Class<?> class_json = null;
        if(tokens.get(0).getType() == TokenType.LBRACE || tokens.get(0).getType() == TokenType.LBRACKET){
            String className = tokens.get(3).getId();
            try {
                class_json = Class.forName(className);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        Object newObj = deserializer.deserialize(class_json);
        return newObj;
    }

    /**
     * This method writes a string to a file.
     * @param fileName is the name of the file
     * @param str is the string to be written.
     */
    public void writeToFile(String fileName, String str){
        try {
            FileWriter writer = new FileWriter(fileName);
            PrintWriter printWriter = new PrintWriter(writer);
            printWriter.write(str);
            printWriter.flush();
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method reads a string from a file.
     * @param fileName is the name of the file
     * @return the string read from a file.
     */
    public String readFromFile(String fileName){
        String string = "";
        try {
            string = new String(Files.readAllBytes(Paths.get(fileName)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return string;
    }
}
