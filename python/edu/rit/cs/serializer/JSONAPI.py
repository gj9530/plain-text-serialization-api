from edu.rit.cs.serializer.Serializer import Serializer
from edu.rit.cs.serializer.Deserializer import Deserilizer
from edu.rit.cs.serializer.Lexer import *
from user.DAG import DAG
import sys


class JSONAPI:
    """
    This class is for users to invoke the functions to serialize or deserialize. It also provides
    the functions to read plain-text from a file or write plain-text to a file.
    """

    def toJson(self, obj):
        """
        Serialize an object to plain-text
        :param obj: the object to be serialized
        :return: the plain-text that the objecst is converted to.
        """
        jsonstring = Serializer().serialize(obj)
        return jsonstring

    def fromJson(self, jsonstring):
        """
        Convert plain-text to an object
        :param jsonstring: the plain-text
        :return: an object
        """
        lexer = Lexer(jsonstring)
        alist = lexer.makestringlist()
        tokens = lexer.maketokens(alist)
        deserializer = Deserilizer(tokens)
        if tokens[0].getType() != TokenType.LBRACE or tokens[0].getType() != TokenType.LBRACE:
            print("The string to be deserialized must start with { or [")
            exit(1)
        class_name = deserializer.rm_class_prefix(tokens[3].getId())
        clazz = getattr(sys.modules[__name__], class_name)
        new_obj = deserializer.deserialize(clazz)
        return new_obj

    def writeToFile(self, jsonstring, filename):
        """
        Write plain-text to a file.
        :param jsonstring: the plain-text
        :param filename: the name of the file
        :return: None
        """
        with open(filename, "w") as f:
            f.write(jsonstring)

    def readFromFile(self, filename):
        """
        Read from a file
        :param filename: the name of the file
        :return: None
        """
        with open(filename) as f:
            jsonstring = f.read()
        return jsonstring



