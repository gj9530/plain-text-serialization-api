import sys

from edu.rit.cs.serializer.Lexer import *
from user.Node import Node
from user.SuperNode import SuperNode
from user.DAG import DAG


class Deserilizer:
    """
    This class deserializes plain-text to objects.
    """

    def __init__(self, tokens):
        self.tokens = self.update_tokens(tokens)
        self.index = 0
        self.token = self.tokens[0]
        self.idCounter_object = {}

    def update_tokens(self, tokens):
        new_tokens = []
        for token in tokens:
            if token.getType() == TokenType.ID:
                id = token.getId()
                new_token = self.java2python(id)
                new_tokens.append(new_token)
            else:
                new_tokens.append(token)
        return new_tokens

    def java2python(self, id):
        """
        This method convert java data types to python data types and create a new token
        with a python data type
        :param id: a string representing data type
        :return: a new token
        """
        datatype = id
        if datatype in ['java.lang.Integer', 'java.lang.Short', 'java.lang.Byte', 'java.lang.Long', 'java.math.BigInteger']:
            datatype = 'int'
        elif datatype in ['java.lang.Float', 'java.lang.Double', 'java.math.BigDecimal', 'java.lang.Number']:
            datatype = 'float'
        elif datatype in ['java.lang.String', 'java.lang.Character']:
            datatype = 'str'
        elif datatype == 'java.lang.Boolean':
            datatype = 'bool'
        elif datatype[0] == '[' or datatype in ['java.util.ArrayList', 'java.util.LinkedList']:
            datatype = 'list'
        elif datatype in ['java.util.HashSet', 'java.util.TreeSet']:
            datatype = 'set'
        elif datatype in ['java.util.HashMap', 'java.util.TreeMap']:
            datatype = 'dict'
        elif datatype == 'java.lang.Object':
            datatype = 'object'
        elif datatype.split('.')[0] == 'java':
            print('Can not handle the data type of', datatype)
            exit(1)
        return Token(TokenType.ID, datatype)

    def previousToken(self):
        """
        This method moves the current token to the previous position.
        :return: None
        """
        self.index -= 1
        if self.index < 0:
            self.index = 0
        self.token = self.tokens[self.index]

    def is_int(self, identifier):
        """
        Check if a string represents an integer
        :param identifier: a string
        :return: True if identifier represents an integer; False otherwise.
        """
        try:
            int(identifier)
            return True
        except ValueError:
            return False

    def is_float(self, identifier):
        """
        Check if a string represents a float
        :param identifier: a string
        :return: True if identifier represents a float; False otherwise.
        """
        try:
            float(identifier)
            return True
        except ValueError:
            return False

    def is_complex(self, identifier):
        """
        Check if a string represents a complex
        :param identifier: a string
        :return: True if identifier represents a complex; False otherwise.
        """
        try:
            complex(identifier)
            return True
        except ValueError:
            return False

    def is_bytes(self, identifier):
        """
        Check if a string represents a bytes
        :param identifier: a string
        :return: True if identifier represents a bytes; False otherwise.
        """
        if len(identifier) < 3:
            return False
        if identifier[0: 2] == 'b\'' and identifier[-1] == '\'':
            return True
        return False

    def is_bytearray(self, identifier):
        """
        Check if a string represents a bytearray
        :param identifier: a string
        :return: True if identifier represents a bytearray; False otherwise.
        """
        if len(identifier) < 11:
            return False
        if identifier[0: 12] == 'bytearray(b\'' and identifier[-2:] == '\')':
            return True
        return False

    def is_memoryview(self, identifier):
        """
        Check if a string represents a memoryview
        :param identifier: a string
        :return: True if identifier represents a memoryview; False otherwise.
        """
        if len(identifier) < 12:
            return False
        if identifier[0: 12] == '<memory at ' and identifier[-1] == '>':
            return True
        return False

    def list2range(self, sequence):
        """
        Convert a list to a range
        :param sequence:  a list
        :return: a range
        """
        if len(sequence) == 1:
            return range(sequence[0], sequence[0])
        step = sequence[1] - sequence[0]
        start = sequence[0]
        end = sequence[-1] + step
        return range(start, end, step)

    def nextToken(self):
        """
        This method moves the current token to the next position.
        :return: None.
        """
        self.index += 1
        if(self.index < len(self.tokens)):
            self.token = self.tokens[self.index]

    def matchToken(self, data_type):
        """
        Check if current token type mataches the given token type.
        :param data_type: the given token type
        :return: current token
        """
        # if self.token.getType() == TokenType.ID:
        #     print(self.token.getType(), '(', self.token.getId(), ')', data_type)
        # else:
        #     print(self.token.getType(), data_type)
        eaten_token = self.token
        if self.token.getType() == data_type:
            self.nextToken()
        else:
            print("The current token type is", self.token.getType(), "which does not match type ", data_type)
            exit(1)
        return eaten_token

    def rm_class_prefix(self, classname):
        """
        Remove the module name of a class
        :param classname: the class name with a module name as a prefix
        :return: the class name without a module name.
        """
        alist = classname.split('.')
        return alist[len(alist) - 1]

    def identify_type(self):
        """
        Identify the data type of current token
        :return: the data type of the token
        """
        if self.token.getType() == TokenType.LBRACE or self.token.getType() == TokenType.LBRACKET:
            self.nextToken()
            self.matchToken(TokenType.CLASS)
            self.matchToken(TokenType.COLON)
            data_type = self.getclass_forname(self.matchToken(TokenType.ID).getId())
            [self.previousToken() for _ in range(4)]
            return data_type
        if self.token.getType() == TokenType.REFID:
            return object
        identifier = self.matchToken(TokenType.ID).getId()
        self.previousToken()
        if identifier[0] == '\"' and identifier[len(identifier) - 1] == '\"':
            return str
        if identifier.lower() == 'true' or identifier == 'false':
            return bool
        if self.is_bytes(identifier):
            return bytes
        if self.is_bytearray(identifier):
            return bytearray
        if self.is_memoryview(identifier):
            return memoryview
        if self.is_int(identifier):
            return int
        if self.is_float(identifier):
            return float
        if self.is_complex(identifier):
            return complex
        return None

    def getclass_forname(self, element_name):
        """
        Return a data type or a class based on a name.
        :param element_name: a name
        :return: a data type or a class
        """
        if element_name == 'null':
            return None
        if element_name == 'list':
            return list
        if element_name == 'tuple':
            return tuple
        if element_name == 'range':
            return range
        if element_name == 'set':
            return set
        if element_name == 'frozenset':
            return frozenset
        if element_name == 'dict':
            return dict
        if element_name == 'int':
            return int
        if element_name == 'float':
            return float
        if element_name == 'str':
            return str
        if element_name == 'complex':
            return complex
        if element_name == 'bytes':
            return bytes
        if element_name == 'bytearray':
            return bytearray
        if element_name == 'memoryview':
            return memoryview
        return getattr(sys.modules[__name__], self.rm_class_prefix(element_name))


    def deserialize_single_value(self, clazz, value):
        """
        Convert the given value from a string type to the correct data type
        :param clazz: the class of the value's data type
        :param value: the value to be converted
        :return: the value with a correct data type
        """
        if issubclass(clazz, bool):
            return bool(value)
        if issubclass(clazz, int):
            return int(value)
        if issubclass(clazz, float):
            return float(value)
        if issubclass(clazz, complex):
            return complex(value)
        if issubclass(clazz, str):
            return value[1: len(value) - 1]


    def deserialize_sequence(self):
        """
        Deserialize a sequence of tokens to a list, a tuple or a range.
        :return:  a list, a tuple or a range
        """
        sequence = []
        self.matchToken(TokenType.LBRACKET)
        self.matchToken(TokenType.CLASS)
        self.matchToken(TokenType.COLON)
        classname = self.matchToken(TokenType.ID).getId()
        self.matchToken(TokenType.COMMA)
        self.matchToken(TokenType.ELEMENTTYPE)
        self.matchToken(TokenType.COLON)
        element_classname = self.matchToken(TokenType.ID).getId()
        if self.token.getType() == TokenType.RBRACKET:
            self.matchToken(TokenType.RBRACKET)
        while self.token.getType() != TokenType.RBRACKET:
            self.matchToken(TokenType.COMMA)
            if element_classname == 'object':
                sequence.append(self.deserialize(self.identify_type()))
            else:
                clazz = self.getclass_forname(element_classname)
                sequence.append(self.deserialize(clazz))
        self.matchToken(TokenType.RBRACKET)
        if classname == 'tuple':
            return tuple(sequence)
        if classname == 'list':
            return list(sequence)
        if classname == 'range':
            return self.list2range(sequence)
        if classname == 'set':
            return set(sequence)
        if classname == 'frozenset':
            return frozenset(sequence)
        if classname == 'memoryview':
            return memoryview(bytearray(sequence))
        return frozenset(sequence)


    def deserialize_dict(self):
        """
        Desesialize a sequence of tokens to a dict
        :return: a dict
        """
        a_dict = {}
        self.matchToken(TokenType.LBRACE)
        self.matchToken(TokenType.CLASS)
        self.matchToken(TokenType.COLON)
        self.matchToken(TokenType.ID)
        self.matchToken(TokenType.COMMA)
        self.matchToken(TokenType.KEYTYPE)
        self.matchToken(TokenType.COLON)
        key_type_name = self.matchToken(TokenType.ID).getId()
        self.matchToken(TokenType.COMMA)
        self.matchToken(TokenType.VALUETYPE)
        self.matchToken(TokenType.COLON)
        value_type_name = self.matchToken(TokenType.ID).getId()
        if self.token.getType() == TokenType.RBRACE:
            self.matchToken(TokenType.RBRACE)
            return {}
        while self.token.getType() != TokenType.RBRACE:
            self.matchToken(TokenType.COMMA)
            if key_type_name == 'object':
                key = self.deserialize(self.identify_type())
            else:
                key = self.deserialize(self.getclass_forname(key_type_name))
            self.matchToken(TokenType.COLON)
            if value_type_name == 'object':
                value = self.deserialize(self.identify_type())
            else:
                value = self.deserialize(self.getclass_forname(value_type_name))
            a_dict[key] = value
        self.matchToken(TokenType.RBRACE)
        return a_dict


    def deserialize_bytes(self):
        """
        Convert a token to a bytes or a bytearray.
        :return: a bytes or a bytearray
        """
        identifier = self.token.getId()
        if self.identify_type() == bytes:
            a_string = identifier[2: len(identifier) - 1]
            self.matchToken(TokenType.ID)
            return a_string.encode()
        self.matchToken(TokenType.ID)
        a_string = identifier[12: len(identifier) - 2]
        return bytearray(a_string.encode())

    def deserialize_object(self, clazz):
        """
        Deserailize a sequence of tokens to an instance of a user-defined class
        :param clazz:  the class of the object
        :return: an instance of a user-defined class
        """
        if self.token.getType() == TokenType.REFID:
            counter = int(self.token.getId())
            self.matchToken(TokenType.REFID)
            return self.idCounter_object[counter]

        self.matchToken(TokenType.LBRACE)
        self.matchToken(TokenType.CLASS)
        self.matchToken(TokenType.COLON)
        class_name = self.matchToken(TokenType.ID).getId()
        self.matchToken(TokenType.COMMA)
        self.matchToken(TokenType.NEWREF)
        self.matchToken(TokenType.COLON)
        id_counter = int(self.matchToken(TokenType.ID).getId())
        new_clazz = self.getclass_forname(class_name)
        new_obj = new_clazz()
        self.idCounter_object[id_counter] = new_obj
        if self.token.getType() != TokenType.RBRACE:
            self.matchToken(TokenType.COMMA)

        for key in new_obj.__dict__.keys():
            self.matchToken(TokenType.ID)
            self.matchToken(TokenType.COLON)
            value_type = self.identify_type()
            new_obj.__dict__[key] = self.deserialize(value_type)
            if self.token.getType() != TokenType.RBRACE:
                self.matchToken(TokenType.COMMA)
        self.matchToken(TokenType.RBRACE)
        return new_obj



    def deserialize(self, clazz):
        """
        Get value based on the current token by identifying the data type according to the given class,
         and invoking  the corresponding method according to the data type
        :param clazz: the given class
        :return: the value of the current token or a sequence of the tokens staring at the current token
        """
        if self.token.getType() == TokenType.ID and self.token.getId() == 'null':
            self.matchToken(TokenType.ID)
            return None

        if self.token.getType() == TokenType.REFID:
            counter = int(self.token.getId())
            self.matchToken(TokenType.REFID)
            return self.idCounter_object.get(counter)
        if issubclass(clazz, (int, float, complex, str, bool)):
            value = self.deserialize_single_value(clazz, self.matchToken(TokenType.ID).getId())
        elif issubclass(clazz, (list, tuple, range, set, frozenset, memoryview)):
            value = self.deserialize_sequence()
        elif issubclass(clazz, dict):
            value = self.deserialize_dict()
        elif issubclass(clazz, (bytes, bytearray)):
            value = self.deserialize_bytes()
        else:
            value = self.deserialize_object(clazz)
        return value





