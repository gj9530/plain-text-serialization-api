import array

class Serializer:
    """
    This class serializes objects to plain-text.
    """
    def __init__(self):
        self.level = 0
        self.idcounter = 0
        self.object_id = {}

    def inclevel(self):
        """
        Increase the level of indentation
        :return: None
        """
        self.level += 1

    def declevel(self):
        """
        Decrease the level of indentation.
        :return:
        """
        self.level -= 1

    def printInc(self):
        """
        Return a number of whitespaces (indentation) based on the level of indentation
        :return:
        """
        return "  " * self.level

    def is_primitive(self, value):
        """
        Check if the given value is a primitive.
        :param value: the given value
        :return: True if the value is a primitive, False otherwise
        """
        primitive = (int, bool, float, complex)
        return isinstance(value, primitive)

    def addQuote(self, var):
        """
        Add double quotes to the value in a string type.
        :param var: the given value
        :return: the string of the value with double quotes.
        """
        return "\"" + str(var) + "\""

    def get_prefix_classname(self, obj):
        """
        Add package name to the class name of the given object.
        :param obj: the given object
        :return: the full name of the class of the object
        """
        module_name_list = str(obj.__module__).split('.')
        prefix = ''
        for i in range(len(module_name_list) - 1):
            prefix += module_name_list[i]
        if len(prefix) == 0:
            return obj.__class__.__name__
        return prefix + '.' + obj.__class__.__name__

    def serialize(self, obj):
        """
        Serialize the given object to plain-text.
        :param obj: the object to be serialized
        :return: the plain-text that the object is converted to
        """
        if obj is None or obj == object:
            return "null"
        if self.is_primitive(obj) or isinstance(obj, (bytes, bytearray)):
            return str(obj)
        if isinstance(obj, str):
            return self.addQuote(obj)
        if isinstance(obj, (list, set, frozenset, tuple, array.ArrayType, range, memoryview)):
            return self.serializeSequence(obj)
        if isinstance(obj, dict):
            return self.serializeDict(obj)

        if obj in self.object_id:
            return "(@referenceID) = " + str(self.object_id[obj])

        return self.serializeObject(obj)

    def serializeSequence(self, obj):
        """
        Serialize the given object that is a sequence type (list, tuple or range) or a set type.
        :param obj: the object to be serialized
        :return: the plain-text that the object is converted to
        """
        jsonstring = "[\n"
        self.level += 1
        jsonstring = jsonstring + self.printInc() + self.addQuote("@class") + ": " + obj.__class__.__name__ + ",\n"
        elementType = None
        for x in obj:
            if x is None:
                continue
            if elementType is None or issubclass(elementType, x.__class__):
                elementType = x.__class__
            else:
                elementType = object
                break
        if elementType is None:
            elementType_name = 'null'
        elif elementType is str:
            elementType_name = 'str'
        else:
            elementType_name = elementType.__name__
        jsonstring = jsonstring + self.printInc() + self.addQuote("@elementType") + ": " + elementType_name + ",\n"
        for x in obj:
            jsonstring = jsonstring + self.printInc() + self.serialize(x) + ",\n"
        jsonstring = jsonstring[0: len(jsonstring) - 2] + "\n"
        self.level -= 1
        jsonstring = jsonstring + self.printInc() + "]"
        return jsonstring

    def serializeDict(self, obj):
        """
        Serialize a dict.
        :param obj: the object to be serialized
        :return: the plain-text that the object is converted to
        """
        jsonstring = "{\n"
        self.level += 1
        jsonstring = jsonstring + self.printInc() + self.addQuote("@class") + ": " + obj.__class__.__name__ + ",\n"
        keyType, valueType = None, None
        for key in obj.keys():
            if key is None:
                continue
            if keyType is None or issubclass(keyType, key.__class__):
                keyType = key.__class__
            else:
                keyType = object
                break
        for value in obj.values():
            if value is None:
                continue
            if valueType is None or issubclass(valueType, value.__class__):
                valueType = value.__class__
            else:
                valueType = object
                break
        if keyType is None:
            keyType_name = 'null'
        elif keyType is str:
            keyType_name = 'str'
        else:
            keyType_name = keyType.__name__
        if valueType is None:
            valueType_name = 'null'
        elif valueType is str:
            valueType_name = 'str'
        else:
            valueType_name = valueType.__name__
        jsonstring = jsonstring + self.printInc() + self.addQuote("@keyType") + ": " + keyType_name + ",\n"
        jsonstring = jsonstring + self.printInc() + self.addQuote("@valueType") + ": " + valueType_name + ",\n"
        for key, value in obj.items():
            serializedKey = self.serialize(key)
            serializedValue = self.serialize(value)
            jsonstring = jsonstring + self.printInc() + serializedKey + ": " + serializedValue+ ",\n"
        jsonstring = jsonstring[0: len(jsonstring) - 2] + "\n"
        jsonstring = jsonstring + self.printInc() + "}"
        return jsonstring

    def serializeObject(self, obj):
        """
        Serialize an object.
        :param obj: the object to be serialized
        :return: the plain-text that the object is converted to
        """
        jsonstring = "{\n"
        self.level += 1
        jsonstring = jsonstring + self.printInc() + self.addQuote("@class") + ": " + self.get_prefix_classname(obj) + ",\n"
        jsonstring = jsonstring + self.printInc() + self.addQuote("@referenceID") + ": " + str(self.idcounter) + ",\n"
        self.object_id[obj] = self.idcounter
        self.idcounter += 1
        for key, value in obj.__dict__.items():
            serializedValue = self.serialize(value);
            jsonstring = jsonstring + self.printInc() + self.addQuote(str(key)) + ": " + serializedValue + ",\n"
        jsonstring = jsonstring[0: len(jsonstring) - 2] + "\n"
        self.level -= 1
        jsonstring = jsonstring + self.printInc() + "}"
        return jsonstring





