from DAG import DAG
from edu.rit.cs.serializer.JSONAPI import JSONAPI
import sys


class Main:


    def main(self):
        api = JSONAPI()
        d = DAG()
        d.makeDAG()
        jsonstring = api.toJson(d)
        print(jsonstring)
        print('==============================================')
        api.writeToFile(jsonstring, 'C:/Users/jingu/IdeaProjects/JSON/json.txt')
        api2 = JSONAPI()
        new_d = api2.fromJson(jsonstring)
        api3 = JSONAPI()
        new_jsonstring = api3.toJson((new_d))
        print(new_jsonstring)
        print('==============================================')
        if jsonstring == new_jsonstring:
            print('jsonstring == new_jsonstring')
        else:
            print('jsonstring != new_jsonstring')




if __name__ == '__main__':
    m = Main()
    m.main()


