from Node import Node
from SuperNode import SuperNode


class DAG:

    def __init__(self):
        # self.aChar = None
        # self.s = None
        # self.i = None
        # self.b = None
        # self.aDouble = None
        # self.aShort = None
        # self.aByte = None
        # self.aLong = None
        # self.value = None
        # self.number = None
        # self.integer = None
        # self.aFloat = None
        # self.n1 = None
        # self.n2 = None
        # self.n3 = None
        # self.aMap = None
        # self.id_node = None
        # self.aset = None
        # self.bset = None
        # self.aList = None
        # self.bList = None
    #=================================================================
        self.num1 = 999
        self.num2 = 3.1415926
        self.a_string = 'xyz'
        self.boo = True
        self.comp = 1 + 2j
        self.num = float(1)
        self.b1 = b'abc'
        self.b2 = bytearray([97, 98, 99])
        self.b3 = memoryview(self.b2)
        self.adict = {}
        self.n1 = None
        self.n2 = None
        self.list1 = None
        self.list2 = {}
        self.obj = object
        self.b = None
        self.tup = None
        self.ran = range(2, 10, 3)
        self.s1 = None
        self.s2 = None


    def makeDAG(self):


        self.n1 = Node(1111)
        self.n2 = Node(2222)
        n3 = Node(3333)
        self.adict['a'] = 1
        self.adict['b'] = 2
        self.n1.neighbor = n3
        self.n2.neighbor = n3
        n3.neighbor = self.n1
        # a = Node(1)
        # b = SuperNode(111)
        self.list1 = [1, 2.1, None, "abc", [1, 2, 3], (10, 11, 12), {"x": 1, "y": 2, "z": None}]
        # self.list2 = [a, b]
        # self.obj = Node(100)
        self.tup = (10, 20, 30)
        self.s1 = set([1, 2, 2, 3])
        self.s2 = frozenset([1, 2, 2, 3])




