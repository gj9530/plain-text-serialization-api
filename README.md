**Goal:**

* This API is used to serialize objects to JSON and deserialize JSON back to objects.  
* It consists of two implementations, one implemented in Java and the other one implemented in Python.   
* Both implementations can work on the same plain-text representation, with some limitations.  
* This package can properly handle the multiple reference problems caused by repeating objects and circular references.  

**Design of the package:**

Both implementations consist of six classes, namely, JSONAPI, Serializer, Deserializer, Lexer, Token and Token-Type.    
JSONAPI is the class exposed to users allowing users to perform JSON serialization and deserialization.   

**How to use:**

Call the toJson(Object object) function in the JSONAPI class to serialize objects to JSON formatted strings and call the fromJson(String jsonString) function to deserialize a JSON string back to an object.   